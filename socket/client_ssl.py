import socket
import ssl


def parsed_url(url):
    """

    :param url:
    :return:
    """
    # 检查协议
    protocol = 'http'
    if url[:7] == 'http://':
        u = url.split('://')[1]
    elif url[:8] == 'https://':
        protocol = 'https'
        u = url.split('://')[1]
    else:
        u = url

    # 检查默认 path
    i = u.find('/')
    if i == -1:
        host = u
        path = '/'
    else:
        host = u[:i]
        path = u[i:]

    # 检查端口
    port_dict = {
        'http': 80,
        'https': 443
    }

    port = port_dict[protocol]
    if ':' in host:
        h = host.split(':')
        host = h[0]
        port = int(h[1])

    return protocol, host, port, path


def get(url):
    """
    GET请求URL并返回响应
    :param url:
    :return:
    """
    protocol, host, port, path = parsed_url(url)

    s = socket_by_protocol(protocol)

    request = """
    GET {} HTTP/1.1\r\n
    host: {}\r\n
    Connection: close\r\n\r\n
    """.format(path, host)
    encoding = 'utf-8'
    s.send(request.encode(encoding))

    response = response_by_socket(s)
    r = response.decode(encoding)

    status_code, headers, body = parsed_response(r)
    if status_code in [301, 302]:
        url = headers['Location']
        return get(url)

    return status_code, headers, body


def socket_by_protocol(protocol):
    """
    根据一个协议返回一个socket实例
    :param protocol:
    :return:
    """
    if protocol == 'http':
        s = socket.socket()
    else:
        s = ssl.wrap_socket(socket.socket())
    return s


def response_by_socket(s):
    """
    返回socket读取的所有数据
    :param s:
    :return:
    """
    response = b''
    buffer_size = 1024
    while True:
        r = s.recv(buffer_size)
        if len(r) == 0:
            break
        response += r
    return response


def parsed_response(r):
    """
    解析response
    :param r:
    :return:
    """
    header, body = r.split('\r\n\r\n', 1)  # 1是指只split一次
    h = header.split('\r\n')
    # 响应行 HTTP/1.1 200 OK
    status_code = h[0].split()[1]
    status_code = int(status_code)

    headers = {}
    for line in h[1:]:
        k, v = line.split(':')
        headers[k] = v

    return status_code, headers, body


def test_parsed_url():
    """
    单元测试
    :return:
    """
    http = 'http'
    https = 'https'
    host = 'g.cn'
    path = '/'
    test_items = [
        ('http://g.cn', (http, host, 80, path)),
        ('http://g.cn/', (http, host, 80, path)),
        ('http://g.cn:90', (http, host, 90, path)),
        ('http://g.cn:90/', (http, host, 90, path)),
        ('https://g.cn', (https, host, 443, path)),
        ('https://g.cn:233', (https, host, 233, path)),
    ]
    for t in test_items:
        url, expected = t
        u = parsed_url(url)
        e = "parsed_url ERROR, {} {}".format(u, expected)
        assert u == expected, e


def test_get():
    """
    测试get方法
    :return:
    """
    urls = [
        'http://movie.douban.com/top250',
        'https://movie.douban.com/top250'
    ]
    for u in urls:
        get(u)


if __name__ == '__main__':
    # test_parsed_url()
    test_get()
