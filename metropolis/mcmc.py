from __future__ import division
import matplotlib.pyplot as plt
import numpy as np
from array import array


def mcmc(p, N=10000, Nlmax=10000, isHM=True):
    A = np.array([p for y in range(len(p))], dtype=np.float64)
    # print(A)
    X0 = np.random.randint(len(p))  # 生成一个0-(len(p)-1)的随机数
    # print(len(p))
    # print(X0)
    count = 0
    samplecount = 0
    L = array("d", [X0])
    l = array("d")
    # print(L)
    # print(l)

    while True:
        X = int(L[samplecount])
        sample = np.random.multinomial(1, A[X])     # 根据上一个样本的多项式分布抽取一个样本，例如[0 0 1]就是抽到了状态为2的膀臂
        cur = np.argmax(sample)     # 返回抽取到的样本的状态（索引），对应0，1，2
        # print(sample)
        # print(cur)
        count += 1
        if isHM:
            a = (p[cur] * A[cur][X] / (p[X] * A[X][cur]))   # 计算拒绝率
            alpha = min(a, 1)
        else:
            alpha = p[cur] * A[cur][X]

        u = np.random.uniform(0, 1)
        if u <= alpha:
            samplecount += 1
            L.append(cur)
            if count > N:
                l.append(cur)
        if len(l) >= Nlmax:
            break
        else:
            continue

    La = np.frombuffer(L)
    la = np.frombuffer(l)
    return La, la


def count(q, n):
    L = array("d")
    l1 = array("d")
    l2 = array("d")
    for e in q:
        L.append(e)
    for e in range(n):
        l1.append(L.count(e))
    for e in l1:
        l2.append(e / sum(l1))
    return l1, l2


p = np.array([0.6, 0.3, 0.1])
a = mcmc(p)[1]
l1 = ['state%d'% x for x in range(len(p))]
plt.pie(count(a,len(p))[0],labels=l1,labeldistance=0.3,autopct='%1.2f%%')
plt.title("sampling")
plt.show()
