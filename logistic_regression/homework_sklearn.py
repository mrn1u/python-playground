from logistic_regression.util import *
from sklearn import linear_model, datasets
from sklearn.metrics import classification_report

iris = datasets.load_iris()
X = iris.data[:, :2]  # we only take the first two features.
Y = iris.target

X_train, X_test, Y_train, Y_test = load_data("KaggleCredit2.csv", "SeriousDlqin2yrs", 0.33)
logreg = linear_model.LogisticRegression()
logreg.fit(X_train.T, Y_train.T)
pred = logreg.predict(X_test.T)
print(classification_report(Y_test.T, pred))
