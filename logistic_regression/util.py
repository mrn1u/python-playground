import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split

def sigmoid(z):
    return np.longfloat(1 / (1 + np.exp(-z)))

def load_data(csv_path, y_column, test_size):
    """
    加载数据数据
    :param csv_path:
    :param y_column:
    :param test_size:
    :return:
    """
    data = pd.read_csv(csv_path, index_col=0)
    data.dropna(inplace=True)  # 过滤空数据

    y = data[y_column]
    X = data.drop(y_column, axis=1)
    X['MonthlyIncome'] /= 10000
    X['age'] /= 10
    # 数据集划分
    Xtr, Xte, ytr, yte = train_test_split(X, y, test_size=test_size)
    ytr = ytr.values.reshape(1, -1)
    yte = yte.values.reshape(1, -1)
    return Xtr.T, Xte.T, ytr, yte