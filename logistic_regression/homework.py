# Author By MrN1u
from logistic_regression.util import *
pd.set_option("display.max_columns", 500)
np.set_printoptions(threshold=np.nan)

def init_parameters(feature_size):
    """
    初始化参数
    :param feature_size:
    :return:
    """
    W_var = np.random.randn(1, feature_size) / 1000
    b_var = np.zeros([1, 1])
    return W_var, b_var

def forward_propagation(X, W, b):
    """
    正想传播
    :param X:
    :param W:
    :param b:
    :return:
    """
    Z = np.dot(W, X) + b
    A = sigmoid(Z)
    # print(A)
    # print(A.shape)
    return A

def compute_loss(A, Y, W, lambd):
    """
    带有L2正则的损失函数
    :param A:
    :param Y:
    :param W:
    :param lambd:
    :return:
    """
    m = A.shape[1]
    cross_entropy_cost = 1. / (-m) * np.sum(np.multiply(Y, np.log(A)) + np.multiply((1 - Y), np.log(1 - A)))
    L2_regression = (lambd / (2. * m)) * np.sum(np.square(W))
    cost = cross_entropy_cost + L2_regression
    cost = np.squeeze(cost)
    return cost

def backward_propagation(A, Y, X, lambd):
    m = A.shape[1]
    dZ = A - Y
    dW = (lambd / m) * W + (1. / m) * np.dot(dZ, X.T)
    db = (1. / m) * np.sum(dZ, axis=1, keepdims=True)
    return dW, db

def update_parameters(W, b, dW, db, learning_rate):
    W = W - learning_rate * dW
    b = b - learning_rate * db
    return W, b

def model(X, Y, params, learning_rate=0.01, iterations=10000, lambd=0.8):
    W = params["W"]
    b = params["b"]
    costs = []
    for i in range(iterations):
        A = forward_propagation(X, W, b)
        cost = compute_loss(A, Y, W, 0.8)
        costs.append(cost)
        dW, db = backward_propagation(A, Y, X, lambd)
        W, b = update_parameters(W, b, dW, db, learning_rate)
        if i % 100 == 0:
            print("cost is: " + str(cost))
    return {
        "W": W,
        "b": b
    }

if __name__ == "__main__":
    X_train, X_test, Y_train, Y_test = load_data("KaggleCredit2.csv", "SeriousDlqin2yrs", 0.33)
    W, b = init_parameters(X_train.shape[0])
    parameters = {
        "W": W,
        "b": b
    }
    parameters = model(X_train, Y_train, parameters, learning_rate=0.001, iterations=5000)
    Y_hat = forward_propagation(X_test, parameters["W"], parameters["b"])
    Y_hat[Y_hat < 0.5] = 0
    Y_hat[Y_hat >= 0.5] = 1
    right = np.sum(Y_hat == Y_test)
    total = Y_test.shape[1]
    accuracy = right / total
    print(accuracy)


