from sklearn.datasets import load_boston
from sklearn.ensemble import RandomForestRegressor
from sklearn.model_selection import train_test_split
from sklearn.metrics import mean_absolute_error

boston_house = load_boston()
boston_features = boston_house.data
boston_labels = boston_house.target
train_features, test_features, train_labels, test_labels = train_test_split(boston_features, boston_labels, test_size=0.33)

rgs = RandomForestRegressor(n_estimators=15, max_features=10, max_depth=4)
rgs.fit(train_features, train_labels)

pred = rgs.predict(test_features)
print(mean_absolute_error(test_labels, pred))
