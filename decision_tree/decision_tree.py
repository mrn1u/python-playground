import pandas as pd
from sklearn import preprocessing
from sklearn import tree
from sklearn.model_selection import train_test_split
from sklearn.metrics import classification_report
import pydotplus
from IPython.display import Image, display

adult_data = pd.read_csv("DecisionTree.csv")
features = adult_data.drop("income", axis=1)
labels = adult_data["income"]

# 编码
features = pd.get_dummies(features)
lbe = preprocessing.LabelEncoder()
labels = lbe.fit_transform(labels)

# 分割数据集
train_features, test_features, train_labels, test_labels = train_test_split(features, labels, train_size=0.2)

clf = tree.DecisionTreeClassifier(criterion='entropy', max_depth=4)
clf.fit(train_features, train_labels)

pred = clf.predict(test_features)
print(classification_report(test_labels, pred))

# 树节点信息
dot_data = tree.export_graphviz(clf,
                                out_file=None,
                                feature_names=features.columns,
                                class_names=['<=50k', '>50k'],
                                filled=True,
                                rounded=True
                                )
print(dot_data)
